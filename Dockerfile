FROM openjdk:17-jdk-alpine
WORKDIR /hipstercard/hipstercard-card-service
COPY target/hipstercard-card-service.jar hipstercard-card-service.jar
RUN pwd
RUN ls
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "./hipstercard-card-service.jar"]
