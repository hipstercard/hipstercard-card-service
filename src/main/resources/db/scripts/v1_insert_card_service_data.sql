INSERT INTO card_type (name, cashback_percent, min_balance_before_transaction_for_cashback) 
VALUES 
    ('DEFAULT', 0, 0),
    ('HIPSTER_DEFAULT', 1, 30000),
    ('HIPSTER_GOLD', 1.5, 50000),
    ('HIPSTER_PLATINUM', 2.0, 70000),
    ('HIPSTER_BLACK', 2.5, 90000);

INSERT INTO bank_account (account_number, client_id) 
VALUES 
    ('bank-account:85763253-388f-4ab6-bf2f-82c1a1cc9f84', 'client:5a58720f-c906-4e3b-82f0-4b0dc7dfe605'),
    ('bank-account:a1f4d7d8-1a6a-4636-ae55-68cc7606e1e1', 'client:dcc8cf80-0640-47e1-89a0-f3897f392447'),
    ('bank-account:1e0aaf40-5088-4d53-86b5-94e97098f744', 'client:85ac0c56-0d55-47b5-9452-894ffa1011a8'),
    ('bank-account:6cc81406-ac5d-483a-b757-1755da3a309c', 'client:e770fd57-9e27-4c8b-9256-cc3768d13562'),
    ('bank-account:e07f8835-ed24-446e-9838-a791e9a951cc', 'client:f2aa3d27-ffe7-4d78-a217-2c9948bd26fd');

INSERT INTO card (id, card_type_name, bank_account_number) 
VALUES 
    ('card:e67444ca-bbc6-4b64-b5b0-3dc0072b1a0c', 'DEFAULT', 'bank-account:85763253-388f-4ab6-bf2f-82c1a1cc9f84'),
    ('card:0335062d-41eb-420a-9181-0768b6196b51', 'HIPSTER_DEFAULT', 'bank-account:a1f4d7d8-1a6a-4636-ae55-68cc7606e1e1'),
    ('card:7ac18634-995e-4537-8cd0-0b4f3d384a3a', 'HIPSTER_GOLD', 'bank-account:1e0aaf40-5088-4d53-86b5-94e97098f744'),
    ('card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab', 'HIPSTER_PLATINUM', 'bank-account:6cc81406-ac5d-483a-b757-1755da3a309c'),
    ('card:1cdc5f9b-da96-41eb-8812-ea4463ae659a', 'HIPSTER_BLACK', 'bank-account:e07f8835-ed24-446e-9838-a791e9a951cc');
