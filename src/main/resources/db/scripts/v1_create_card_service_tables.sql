CREATE TABLE IF NOT EXISTS card_type (
    name VARCHAR(255) PRIMARY KEY,
    cashback_percent DECIMAL(10, 2) NOT NULL,
    min_balance_before_transaction_for_cashback DECIMAL(10, 2) NOT NULL
);

CREATE TABLE IF NOT EXISTS bank_account (
    account_number VARCHAR(255) PRIMARY KEY,
    client_id VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS card (
    id VARCHAR(255) PRIMARY KEY,
    card_type_name VARCHAR(255) NOT NULL,
    bank_account_number VARCHAR(255) NOT NULL,
    FOREIGN KEY(card_type_name) REFERENCES card_type(name),
    FOREIGN KEY(bank_account_number) REFERENCES bank_account(account_number)
);

DELETE FROM card_type;
DELETE FROM bank_account;
DELETE FROM card;
