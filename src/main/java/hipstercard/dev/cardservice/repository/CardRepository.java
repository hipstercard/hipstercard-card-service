package hipstercard.dev.cardservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import hipstercard.dev.cardservice.domain.BankAccount;
import hipstercard.dev.cardservice.domain.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, String> {
    List<Card> findAllByBankAccount(BankAccount bankAccount);
}
