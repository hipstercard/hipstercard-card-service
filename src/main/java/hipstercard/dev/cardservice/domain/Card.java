package hipstercard.dev.cardservice.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;

@Entity
@Table(name = "card")
@Getter
public class Card {
    @Id
    private String id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "card_type_name", referencedColumnName = "name")
    private CardType cardType;

    @ManyToOne
    @JoinColumn(name = "bank_account_number", referencedColumnName = "account_number", nullable = false)
    private BankAccount bankAccount;
}
