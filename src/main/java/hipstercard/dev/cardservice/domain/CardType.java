package hipstercard.dev.cardservice.domain;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;

@Entity
@Table(name = "card_type")
@Getter
public class CardType {
    @Id
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "cashback_percent", nullable = false)
    private BigDecimal cashbackPercent;

    @Column(name = "min_balance_before_transaction_for_cashback", nullable = false)
    private BigDecimal minBalanceBeforeTransactionForCashback;
}
