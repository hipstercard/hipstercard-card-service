package hipstercard.dev.cardservice.domain;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @Column(name = "account_number", nullable = false, unique = true)
    private String accountNumber;

    @OneToMany(mappedBy = "bankAccount", fetch = FetchType.LAZY)
    private List<Card> cards;

    @Column(name = "client_id", nullable = false)
    private String clientId;
}
