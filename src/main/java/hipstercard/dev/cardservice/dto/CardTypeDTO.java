package hipstercard.dev.cardservice.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardTypeDTO {
    private String name;
    private BigDecimal cashbackPercent;
    private BigDecimal minBalanceBeforeTransactionForCashback;
}
