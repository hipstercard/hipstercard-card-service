package hipstercard.dev.cardservice.api.handling;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import hipstercard.dev.cardservice.error.logic.ErrorCode;
import hipstercard.dev.cardservice.error.logic.ErrorEntity;
import hipstercard.dev.cardservice.error.logic.ErrorResponse;
import hipstercard.dev.cardservice.error.validation.FieldValidationError;
import hipstercard.dev.cardservice.error.validation.ValidationErrorResponse;
import hipstercard.dev.cardservice.exception.BaseException;

@RestControllerAdvice
public class CardRestControllerAdvice {
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleNoHandlerFoundException(NoHandlerFoundException exception) {
        return ErrorResponse.builder()
                .numberOfErrors(1)
                .errors(List.of(ErrorEntity.builder()
                        .errorCode(ErrorCode.NO_HANDLER_FOUND)
                        .date(ZonedDateTime.now())
                        .errorMessage(exception.getMessage())
                        .dataCausedError(exception.getRequestURL())
                        .build()))
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ValidationErrorResponse handleValidationException(MethodArgumentNotValidException ex) {
        List<FieldValidationError> fieldErrors = new ArrayList<>();
        ex.getBindingResult().getAllErrors()
                .forEach((error) -> fieldErrors.add(FieldValidationError.builder()
                        .fieldName(((FieldError) error).getField())
                        .fieldError(error.getDefaultMessage())
                        .build()));

        return ValidationErrorResponse.builder()
                .fieldErrorsNumber(fieldErrors.size())
                .fieldErrors(fieldErrors)
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BaseException.class)
    public ErrorResponse handleBaseException(BaseException exception) {
        return ErrorResponse.builder()
                .numberOfErrors(1)
                .errors(List.of(ErrorEntity.builder()
                        .errorCode(exception.getErrorCode())
                        .date(exception.getDate())
                        .errorMessage(exception.getErrorMessage())
                        .dataCausedError(exception.getDataCausedError())
                        .build()))
                .build();
    }
}
