package hipstercard.dev.cardservice.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hipstercard.dev.cardservice.dto.BankAccountCardsDTO;
import hipstercard.dev.cardservice.dto.BankAccountDTO;
import hipstercard.dev.cardservice.service.CardService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/cards")
@RequiredArgsConstructor
public class CardRestController {
    private final CardService cardService;

    @PostMapping("/get-linked-cards")
    public BankAccountCardsDTO getLinkedCards(@Valid @RequestBody BankAccountDTO bankAccountDTO) {
        return cardService.getLinkedCardsInfo(bankAccountDTO);
    }
}
