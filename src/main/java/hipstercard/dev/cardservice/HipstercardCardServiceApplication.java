package hipstercard.dev.cardservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HipstercardCardServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(HipstercardCardServiceApplication.class, args);
	}
}
