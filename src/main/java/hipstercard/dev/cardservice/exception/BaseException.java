package hipstercard.dev.cardservice.exception;

import java.time.ZonedDateTime;

import hipstercard.dev.cardservice.error.logic.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BaseException extends RuntimeException {
    protected ZonedDateTime date;
    protected String errorMessage;
    protected ErrorCode errorCode;
    protected Object dataCausedError;
}
