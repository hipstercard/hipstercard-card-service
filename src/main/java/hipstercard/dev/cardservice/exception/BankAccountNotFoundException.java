package hipstercard.dev.cardservice.exception;

import java.time.ZonedDateTime;

import hipstercard.dev.cardservice.error.logic.ErrorCode;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class BankAccountNotFoundException extends BaseException {
    @Builder
    public BankAccountNotFoundException(ZonedDateTime date, String errorMessage, ErrorCode errorCode,
            Object dataCausedError) {
        super(date, errorMessage, errorCode, dataCausedError);
    }
}
