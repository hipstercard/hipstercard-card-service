package hipstercard.dev.cardservice.error.logic;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
    private int numberOfErrors;
    private List<ErrorEntity> errors;
}
