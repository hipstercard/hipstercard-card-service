package hipstercard.dev.cardservice.error.logic;

import java.time.ZonedDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorEntity {
    private ErrorCode errorCode;
    private ZonedDateTime date;
    private String errorMessage;
    private Object dataCausedError;
}
