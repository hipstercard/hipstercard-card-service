package hipstercard.dev.cardservice.error.logic;

public enum ErrorCode {
    BANK_ACCOUT_NOT_FOUND_BY_NUMBER,
    NO_HANDLER_FOUND,
}
