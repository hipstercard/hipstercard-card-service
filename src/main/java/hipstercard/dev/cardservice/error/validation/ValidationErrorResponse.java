package hipstercard.dev.cardservice.error.validation;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidationErrorResponse {
    private int fieldErrorsNumber;
    private List<FieldValidationError> fieldErrors;
}
