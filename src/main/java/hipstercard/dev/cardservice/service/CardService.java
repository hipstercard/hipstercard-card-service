package hipstercard.dev.cardservice.service;

import hipstercard.dev.cardservice.dto.BankAccountCardsDTO;
import hipstercard.dev.cardservice.dto.BankAccountDTO;

public interface CardService {
    BankAccountCardsDTO getLinkedCardsInfo(BankAccountDTO bankAccountDTO);
}
