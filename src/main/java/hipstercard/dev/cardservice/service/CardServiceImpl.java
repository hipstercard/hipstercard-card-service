package hipstercard.dev.cardservice.service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import hipstercard.dev.cardservice.domain.BankAccount;
import hipstercard.dev.cardservice.domain.Card;
import hipstercard.dev.cardservice.dto.BankAccountCardsDTO;
import hipstercard.dev.cardservice.dto.BankAccountDTO;
import hipstercard.dev.cardservice.dto.CardInfoDTO;
import hipstercard.dev.cardservice.dto.CardTypeDTO;
import hipstercard.dev.cardservice.error.logic.ErrorCode;
import hipstercard.dev.cardservice.exception.BankAccountNotFoundException;
import hipstercard.dev.cardservice.repository.BankAccountRepository;
import hipstercard.dev.cardservice.repository.CardRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {
    private final CardRepository cardRepository;
    private final BankAccountRepository bankAccountRepository;

    @Override
    public BankAccountCardsDTO getLinkedCardsInfo(BankAccountDTO bankAccountDTO) {
        Optional<BankAccount> bankAccount = bankAccountRepository
                .findByAccountNumber(bankAccountDTO.getBankAccountNumber());

        if (bankAccount.isEmpty()) {
            throw BankAccountNotFoundException.builder()
                    .date(ZonedDateTime.now())
                    .errorMessage("Can not find bank account with this account number!")
                    .errorCode(ErrorCode.BANK_ACCOUT_NOT_FOUND_BY_NUMBER)
                    .dataCausedError(bankAccountDTO)
                    .build();
        }

        List<Card> linkedCards = cardRepository.findAllByBankAccount(bankAccount.get());
        return BankAccountCardsDTO.builder()
                .cards(convertLinkedCards(linkedCards))
                .build();
    }

    private List<CardInfoDTO> convertLinkedCards(List<Card> linkedCards) {
        List<CardInfoDTO> cardInfoSet = new ArrayList<>();
        for (Card card : linkedCards) {
            cardInfoSet.add(convertCard(card));
        }

        return cardInfoSet;
    }

    private CardInfoDTO convertCard(Card card) {
        CardTypeDTO cardTypeDTO = CardTypeDTO.builder()
                .name(card.getCardType().getName())
                .cashbackPercent(card.getCardType().getCashbackPercent())
                .minBalanceBeforeTransactionForCashback(card.getCardType().getMinBalanceBeforeTransactionForCashback())
                .build();

        return CardInfoDTO.builder()
                .id(card.getId())
                .cardType(cardTypeDTO)
                .build();
    }
}
