package hipstercard.dev.cardservice.api;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import hipstercard.dev.cardservice.dto.BankAccountCardsDTO;
import hipstercard.dev.cardservice.dto.BankAccountDTO;
import hipstercard.dev.cardservice.dto.CardInfoDTO;
import hipstercard.dev.cardservice.dto.CardTypeDTO;
import hipstercard.dev.cardservice.error.logic.ErrorCode;
import hipstercard.dev.cardservice.error.logic.ErrorResponse;
import hipstercard.dev.cardservice.error.validation.FieldValidationError;
import hipstercard.dev.cardservice.error.validation.ValidationErrorResponse;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@RequiredArgsConstructor
@TestInstance(Lifecycle.PER_CLASS)
public class CardRestControllerTest {
    @LocalServerPort
    private int port;

    @BeforeAll
    public void setUpRestAssured() {
        RestAssured.port = port;
        RestAssured.baseURI = "http://localhost:" + port +
                "/api/v1/cards/get-linked-cards";
    }

    @Test
    public void testInvalidMapping() {
        Response response = RestAssured.when().get("http://localhost:" + port +
                "/some-invalid-mapping");

        ErrorResponse errorResponse = response.getBody().as(ErrorResponse.class);

        Assertions.assertEquals(404, response.getStatusCode());
        Assertions.assertEquals(1, errorResponse.getNumberOfErrors());
        Assertions.assertEquals(ErrorCode.NO_HANDLER_FOUND,
                errorResponse.getErrors().get(0).getErrorCode());
    }

    @Test
    public void testInvalidBankAccountNumber() {
        BankAccountDTO bankAccountDTO = BankAccountDTO.builder()
                .bankAccountNumber("Some-invalid-bank-accont-number")
                .build();

        Response response = RestAssured.given().contentType("application/json")
                .body(bankAccountDTO).when().post();

        ValidationErrorResponse validationErrorResponse = response.getBody().as(ValidationErrorResponse.class);

        List<FieldValidationError> expectedFieldErrors = List.of(FieldValidationError.builder()
                .fieldName("bankAccountNumber")
                .fieldError("Bank account number should be in format bank-account:{UUID}")
                .build());

        ValidationErrorResponse expected = ValidationErrorResponse.builder()
                .fieldErrorsNumber(1)
                .fieldErrors(expectedFieldErrors)
                .build();

        Assertions.assertEquals(400, response.getStatusCode());
        Assertions.assertEquals(expected, validationErrorResponse);
    }

    @Test
    public void testExistingBankAccountNumber() {
        BankAccountDTO bankAccountDTO = BankAccountDTO.builder()
                .bankAccountNumber("bank-account:e07f8835-ed24-446e-9838-a791e9a951cc")
                .build();

        Response response = RestAssured.given().contentType("application/json")
                .body(bankAccountDTO).when().post();

        BankAccountCardsDTO bankAccountCardsDTO = response
                .getBody().as(BankAccountCardsDTO.class);

        CardTypeDTO expectedCardType = CardTypeDTO.builder()
                .name("HIPSTER_BLACK")
                .cashbackPercent(new BigDecimal("2.50"))
                .minBalanceBeforeTransactionForCashback(new BigDecimal("90000.00"))
                .build();

        CardInfoDTO expectedCardInfo = CardInfoDTO.builder()
                .id("card:1cdc5f9b-da96-41eb-8812-ea4463ae659a")
                .cardType(expectedCardType)
                .build();

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals(expectedCardInfo,
                bankAccountCardsDTO.getCards().get(0));
    }

    @Test
    public void testBlankBankAccountNumber() {
        BankAccountDTO bankAccountDTO = BankAccountDTO.builder()
                .bankAccountNumber("")
                .build();

        Response response = RestAssured.given().contentType("application/json")
                .body(bankAccountDTO).when().post();

        ValidationErrorResponse validationErrorResponse = response.getBody().as(ValidationErrorResponse.class);

        List<FieldValidationError> expectedFieldErrors = List.of(FieldValidationError.builder()
                .fieldName("bankAccountNumber")
                .fieldError("Bank account number should not be blank!")
                .build(),
                FieldValidationError.builder()
                        .fieldName("bankAccountNumber")
                        .fieldError("Bank account number should be in format bank-account:{UUID}")
                        .build());

        Assertions.assertEquals(400, response.getStatusCode());
        Assertions.assertEquals(2, validationErrorResponse.getFieldErrorsNumber());
        Assertions.assertTrue(expectedFieldErrors.size() == validationErrorResponse.getFieldErrors().size() &&
                expectedFieldErrors.containsAll(validationErrorResponse.getFieldErrors()) &&
                validationErrorResponse.getFieldErrors().containsAll(expectedFieldErrors));
    }
}
