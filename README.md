# 💳 Cards Service

- This microservice is responsible for storing users' cards and bank accounts.

## 1. 🔨 Entities

- This microservice has three entity classes:

1. [BankAccount](https://gitlab.com/hipstercard/hipstercard-card-service/-/blob/main/src/main/java/hipstercard/dev/cardservice/domain/BankAccount.java)
2. [CardType](https://gitlab.com/hipstercard/hipstercard-card-service/-/blob/main/src/main/java/hipstercard/dev/cardservice/domain/CardType.java)
3. [Card](https://gitlab.com/hipstercard/hipstercard-card-service/-/blob/main/src/main/java/hipstercard/dev/cardservice/domain/Card.java)

- Relationships between entities: multiple default cards can be linked to the same bank account, but only one hipster card can be among them
- Every card can be only of one type

## 2. 💾 Database and migrations

- PostgreSQL is used as main database in this microservice. It is deployed in a docker container on a remote server.
- To work with database in Java code I was using Hibernate ORM and Spring Data JPA
- For migrations I was using Liquibase library. All the migration scripts are stored in [scripts](https://gitlab.com/hipstercard/hipstercard-card-service/-/tree/main/src/main/resources/db/scripts) folder.
- The database structure can be shown in the following diagram:

![cards-tables.jpg](src/main/resources/readme-images/cards-tables.jpg)

## 3. 📥 Endpoints

- This microservice has only one endpoint: **/api/v1/cards/get-linked-cards**
- Endpoint has the following logic:

- It receives bank account number in JSON format, for example:

```json
{
    "bankAccountNumber": "bank-account:e07f8835-ed24-446e-9838-a791e9a951cc"
}
```

- Further, thanks to Spring Data JPA, an object of the BankAccount class is retrieved from the database.
- After that, again thanks to Spring Data Jpa, all cards linked to this bank account and their card types are retrieved from the database.
- Finally, the List of cards is translated into JSON and returned to the user in the following format with status code `200 OK`:

```json
{
    "cards": [
        {
            "id": "card:1cdc5f9b-da96-41eb-8812-ea4463ae659a",
            "cardType": {
                "name": "HIPSTER_BLACK",
                "cashbackPercent": 2.50,
                "minBalanceBeforeTransactionForCashback": 90000.00
            }
        }
    ]
}
```

## 4. ✅ Validation and custom error responses

- For validation I was using Spring Boot Starter Validation.
- If some data send to a microservice is not valid (for example bank account number is blank) user will receive `400 BAD REQUEST` status code and custom erorr response in JSON format like this:

```json
{
    "fieldErrorsNumber": 2,
    "fieldErrors": [
        {
            "fieldName": "bankAccountNumber",
            "fieldError": "Bank account number should not be blank!"
        },
        {
            "fieldName": "bankAccountNumber",
            "fieldError": "Bank account number should be in format bank-account:{UUID}"
        }
    ]
}
```

- If the data is valid but there is no bank account with given number in database, user will also receive `400 BAD REQUEST` status code and custom erorr response in JSON format like this:

```json
{
    "numberOfErrors": 1,
    "errors": [
        {
            "errorCode": "BANK_ACCOUT_NOT_FOUND_BY_NUMBER",
            "date": "2023-08-04T13:02:36.592116321+05:00",
            "errorMessage": "Can not find bank account with this account number!",
            "dataCausedError": {
                "bankAccountNumber": "not-existing-bank-account-number"
            }
        }
    ]
}
```

## 5. ♻️ Testing

- For tests I used Testcontainers library. You can see PostgreSQL testcontainer configuration in [application-test.yml](https://gitlab.com/hipstercard/hipstercard-card-service/-/blob/main/src/test/resources/application-test.yml) file. So, all tests connect to PostgreSQL, running in testcontainer, and when the application starts, it is connecting to a PostgreSQL that is running on a remote server (now also in a docker container in an application network)
- Also, for sending http requests in tests I was using RestAssured library. When mvn test phase, thanks to @SpringBootTest microservice starts, and RestAssured send requests to it.

## 6. 🦊 Gitlab CI

- In [.gitlab-ci.yml](https://gitlab.com/hipstercard/hipstercard-card-service/-/blob/main/.gitlab-ci.yml) file I have following jobs and stages:
1. `install_mr` job in `install` stage - it is running when merge request to main is created (GitFlow will be there soon, my bad...). This stage build the microservice and run tests in [src/test](https://gitlab.com/hipstercard/hipstercard-card-service/-/tree/main/src/test) folder by executig `mvn install` command
2. `install` job in `install` stage - it is running when merge request was merged to main. Just do `mvn clean install` and generate .jar artifact that will be used in next stages to build docker image.
3. `docker_work` job in `docker_work` stage - removes unused images, build new docker image using .jar file from previous stage and push this image to a Gitlab container registry
4. `deploy` job in `deploy` stage - stops and removes previous containers, creates network, and start new container from image generated at previous stage.

## 7. 👀 Deploy

- The microservice is deployed now at the remote server. You can access it by sending post http request to **http://45.153.71.217:8082/api/v1/cards/get-linked-cards** with following JSON body:

```json
{
  "bankAccountNumber": "bank-account:6cc81406-ac5d-483a-b757-1755da3a309c"
}
```

- You should receive a `200 OK` response status and following JSON body as a response:

```json
{
  "cards": [
    {
      "id": "card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab",
      "cardType": {
        "name": "HIPSTER_PLATINUM",
        "cashbackPercent": 2.00,
        "minBalanceBeforeTransactionForCashback": 70000.00
      }
    }
  ]
}
```
